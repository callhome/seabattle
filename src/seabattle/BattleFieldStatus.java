package seabattle;

//**********************************************************
//Class BattleFieldStatus
//
//represents base battlefield.
// Track each cell status and ship presence (true/false)
// Used to store opponent battlefield information 
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public class BattleFieldStatus {

	// constant for battlefield size
	private static final int SIZE = 10;
	// two dimensional array to store each cell status
	private CellStatus data[][] = new CellStatus[SIZE][SIZE];

	// *********************************
	// getDimension method:
	// return game field dimension
	//
	// Precondition: data array has been initialized
	// Postcondition:
	// @param none
	//
	// @return game field dimension
	//
	// *********************************
	public static int getDimension() {
		return SIZE;
	}

	// *********************************
	// validateCell method:
	// validate provided coordinates
	//
	// Precondition: data array has been initialized
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	//
	// @return none
	// throw exception for invalid coordinates
	//
	// *********************************
	public void validateCell(int row, int col) throws Exception {
		if (row >= 0 && row < data.length && col >= 0 && col < data.length)
			return;
		else
			throw new Exception("Invalid position");
	}

	// *********************************
	// isEmpty method:
	// Validate cell coordinates and return true if cell is empty (no ship)
	// Cell may have null value
	// Precondition:data array has been initialized
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	//
	// @return true if cell is empty
	//
	// *********************************
	public boolean isEmpty(int row, int col) throws Exception {
		validateCell(row, col);
		return data[row][col] == null || data[row][col].isEmpty();
	}

	// *********************************
	// wasHit method:
	// Validate cell coordinates and return true if cell was hit already
	// Cell may have null value then it is considered empty and not hit
	// Precondition:data array has been initialized
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	//
	// @return true if cell was hit
	//
	// *********************************
	public boolean wasHit(int row, int col) throws Exception {
		validateCell(row, col);
		if (data[row][col] == null)
			return false;
		return data[row][col].wasHit();
	}

	// *********************************
	// updateCell method:
	// Validate cell coordinates and update cell value with new status object
	//
	// Precondition:data array has been initialized
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	//
	// @return none
	//
	// *********************************
	protected void updateCell(int row, int col, CellStatusShip ship) throws Exception {
		validateCell(row, col);
		data[row][col] = ship;
	}

	// *********************************
	// haveShips method:
	// return true if any ship cell(not hit) present
	//
	// Precondition:data array has been initialized
	// Postcondition:
	// @param none
	//
	// @return true if any ship cell(not hit) present
	//
	// *********************************
	public boolean haveShips() {
		for (CellStatus[] row : data) {
			for (CellStatus cell : row) {
				if (cell != null && cell.hasShip() && !cell.wasHit())
					return true;
			}
		}
		return false;
	}

	// *********************************
	// getCellStatusLetter method:
	// Return cell letter representation
	// account for ship and empty cell been hit or not
	//
	// Precondition:
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	//
	// @return letter representing cell status
	//
	// *********************************
	protected char getCellStatusLetter(int row, int col) {
		// If cell is null then it is empty
		if (data[row][col] == null)
			return ' ';
		else
			// get letter from cell instance
			return data[row][col].getCellStatusLetter();
	}

	// *********************************
	// findEmptyCell method:
	// Find any empty cell to use as next move by computer
	// This is used if we can't find cell for next move (by computer) using method
	// in ComputerPlayer class
	//
	// Precondition:data array has been initialized
	// Postcondition:
	// @param none
	//
	// @return true if any ship cell(not hit) present
	//
	// *********************************
	public Move findEmptyCell() {
		// Check every cell on the battlefield
		for (int row = 0; row < data.length; row++) {
			for (int col = 0; col < data[row].length; col++) {
				try {
					// if we have empty and not hit cell then use it for next move
					if (isEmpty(row, col) && !wasHit(row, col))
						return new Move(row, col);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// Nothing works, use 0,0 coordinates
		return new Move(0, 0);
	}

	// *********************************
	// markMiss method:
	// Mark cell with current move coordinates as miss
	// If cell was null or empty constant we create new instance of CellStatus to
	// mark it as 'hit'
	//
	// Precondition:data array has been initialized
	// Postcondition:
	// @param m - move instance with coordinates
	//
	// @return none
	//
	// *********************************
	public void markMiss(Move m) throws Exception {
		int row = m.getRow();
		int col = m.getCol();
		validateCell(row, col);
		if (data[row][col] == null || data[row][col] == CellStatus.getEmptyCell())
			data[row][col] = new CellStatus(true);
		else
			data[row][col].setHit();
	}

	// *********************************
	// markMiss method:
	// Mark cell with current move coordinates as miss
	// If cell was null or empty constant we create new instance of CellStatus to
	// mark it as 'hit' and having ship
	//
	// Precondition:data array has been initialized
	// Postcondition:
	// @param m - move instance with coordinates
	//
	// @return none
	//
	// *********************************
	public void markHit(Move m) throws Exception {
		int row = m.getRow();
		int col = m.getCol();
		validateCell(row, col);
		if (data[row][col] == null || data[row][col] == CellStatus.getEmptyCell())
			data[row][col] = new CellStatus(true, true);
		else
			data[row][col].setHit();
	}
}
