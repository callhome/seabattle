package seabattle;

//**********************************************************
//Enum MoveStatus
//
// represent result of single player move
//
// There are two possibilities - Hit or Miss
// Provides String representation of status as '*' (miss) or 'X' as (hit)
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public enum MoveStatus  {
	// Two possible move results 
	MISS,HIT;
	
	// *********************************
	// toString method:
	// return move status string representation to be displayed on battlefield
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	// 
	// *********************************
	public String toString() {
		switch (this) {
		case MISS: return "*";
		case HIT: return "X";
		//case SUNK: return "X";
		}
		return " ";
	}
}
