package seabattle;

import java.util.Scanner;

//**********************************************************
//Class KeyboardPlayer
//
// represents human/keyboard player 
// Extends generic player 
// implements methods to input ship location and coordinates for each move
//
// Name:Alice Shlykov
//
// Date: 5/10/2018
//
//**********************************************************
public class KeyboardPlayer extends GenericPlayer {

	// Scanner object to read keyboard input
	private Scanner keyb = new Scanner(System.in);

	// *********************************
	// constructor method:
	// Create new instance with given player name
	// Precondition:
	// Postcondition:
	// @param name - player name
	//
	// @return none
	//
	// *********************************

	KeyboardPlayer(String name) {
		super(name);
	}

	// *********************************
	// getNextMove method:
	// Get next move coordinates from keyboard input
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return new move object with coordinates filled
	//
	// *********************************
	@Override
	public Move getNextMove() {
		// Loop in case we have invalid user input
		while (true) {
			// Get row coordinate
			System.out.println("Enter next move row=");
			int row = keyb.nextInt();
			// Get column coordinate
			System.out.print("Enter column:");
			int col = keyb.nextInt();
			try {
				// Check if we already tried these coordinates before using opponent battlefield
				// If not then return new Move object
				if (!getOpponentBattleField().wasHit(row, col))
					return new Move(row, col);
				else
					System.out.println("You have already tried this location.");
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}
	}

	// *********************************
	// placeOneShip method:
	// Place one player ship on the battlefield
	// Ask player for coordinates and ship direction
	// Precondition:
	// Postcondition:
	// @param shipSize - number of cells in the ship
	// @param ship - ship number
	//
	// @return none
	//
	// *********************************
	@Override
	void placeOneShip(int shipSize, int ship) {
		// Loop in case we have invalid input
		while (true) {
			// Display ship size and get input for row coordinate
			System.out.print("Ship size " + shipSize + ". Enter location row:");
			int row = keyb.nextInt();
			// Get column coordinate
			System.out.print("Enter column:");
			int col = keyb.nextInt();
			// get ship direction
			System.out.print("Ship direction vertical (y/n):");
			String s = keyb.next();// keyb.nextLine();
			boolean isVertical = ("y".equalsIgnoreCase(s));
			System.out.println("Placing ship size " + shipSize + " (" + row + "," + col + ") vertical=" + isVertical);
			try {
				// First try to place ship without marking battlefield cells
				// In case we collide with another ship or run out of board
				// it will generate exception and we restart loop asking for new coordinates

				getMyBattleField().placeShip(row, col, isVertical, shipSize, true);
				// If we got there then entered coordinates are valid
				// and we will mark board cells with ship number
				getMyBattleField().placeShip(row, col, isVertical, shipSize, false);
				// Display board to user
				this.showBoard();
				return;
			} catch (Exception e) {
				System.out.println("Invalid input:" + e);
			}
		}
	}

}
