package seabattle;

//**********************************************************
//Class Move
//
//Store board coordinates for each move and result (hit/miss)
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public class Move {
	// Coordinates for current move
	private int row, col;
	// Store move result (hit/miss)
	private MoveStatus result;

	// *********************************
	// constructor method:
	// Create new instance with spcified coordinates
	// Precondition:
	// Postcondition:
	// @param row - row coordinate
	// @param col - col coordinate
	//
	// @return none
	//
	// *********************************
	public Move(int row, int col) {
		this.row = row;
		this.col = col;
	}

	// *********************************
	// setStatus method:
	// accessor method to update move status
	// Precondition:
	// Postcondition:
	// @param status - new move status
	//
	// @return none
	//
	// *********************************
	public void setStatus(MoveStatus status) {
		result = status;
	}

	// *********************************
	// getStatus method:
	// accessor method to get move status
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return move status object
	//
	// *********************************
	public MoveStatus getStatus() {
		return result;
	}
	// *********************************
	// getRow method:
	// accessor method to get row value
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return row coordinate
	//
	// *********************************

	public int getRow() {
		return row;
	}

	// *********************************
	// getCol method:
	// accessor method to get column value
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return column coordinate
	//
	// *********************************
	public int getCol() {
		return col;
	}
	// *********************************
	// toString method:
	// return string representation of move
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return string with coordinates and move status
	//
	// *********************************

	public String toString() {
		return "Move:r=" + row + " c=" + col + " status=" + result;
	}
}
