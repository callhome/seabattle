package seabattle;

//**********************************************************
//Class CellStatusShip
//
//Represents battlefield cell occupied by ship
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************

public class CellStatusShip extends CellStatus {

	// index/number of ship in this cell
	private int shipID;
	// Cell index for multi-cell ship
	private int cellIndex;

	// *********************************
	// constructor method:
	// New instance with provided ship ID and cell index
	//
	// Precondition:
	// Postcondition:
	// @param shipID ship number/id
	// @param cellIndex - cell number/index in multi-cell ship
	//
	// @return none
	//
	// *********************************
	public CellStatusShip(int shipID, int cellIndex) {
		super(false, true);
		this.shipID = shipID;
		this.cellIndex = cellIndex;
	}

	// *********************************
	// setHit method:
	// Mark this cell as hit
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************
	public void setHit() {
		setHit(true);
	}

	// *********************************
	// getCellStatusLetter method:
	// Return letter to display content of current cell
	// it is X if this cell was hit
	// otherwise it is ship index/number
	//
	// Precondition:
	// Postcondition:
	// @param none
	// @param letter will cell status
	//
	// @return none
	//
	// *********************************
	public char getCellStatusLetter() {
		if (this.wasHit()) {
			return 'X';
		} else {
			return (char) ('0' + shipID);
		}
	}

}
