package seabattle;

//**********************************************************
//SeaBattle
//
//Main class for SeaBattle game.
//Create player objects, setup boards and drive game till one of player win
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public class SeaBattle {

	// Array store game players
	// Player with index 0 always represent keyboard/human player (if present)
	private Player players[] = new Player[2];

	// *********************************
	// constructor method:
	//
	// Precondition:
	// Postcondition: Create two player instances
	// @param none
	//
	// @return none
	//
	// *********************************

	public SeaBattle() {
		players[0] = new KeyboardPlayer("Player");
		// For testing we can play game between two computer players
		// comment KeyboardPlayer above and uncomment line below
		// players[0] = new ComputerPlayer("Computer1");
		players[1] = new ComputerPlayer("Computer2");

	}

	// *********************************
	// oneMove method:
	//
	// Implement single game move
	// Player one will produce next move
	// Player two will test move and respond with result (hit/miss)
	//
	// Precondition: Ships placed on battlefield
	// Postcondition: Player move tested and battlefield updated
	// @param one - Player instance making move
	// @param two - second player object
	//
	// @return none
	//
	// *********************************

	public void oneMove(Player one, Player two) {
		// endless loop to address invalid input
		while (true) {
			try {
				// Get next move information from player 'one'
				Move m = one.getNextMove();
				// let player two test move and update status (hit/miss)
				m = two.testMove(m);
				// player one update his board with move results
				one.recordMove(m);
				System.out.println("Player " + one.getName() + " move " + m);
				return;
			} catch (Exception e) {
				System.out.println("Invalid move");
			}
		}
	}

	// *********************************
	// playGame method:
	// Main game method.
	// Ask each player for next move, test each move till one of players win
	// Precondition: Player array initialized, ships placed on each battlefield
	// Postcondition: Game completed
	// @param none
	//
	// @return none
	//
	// *********************************

	public void playGame() {
		// count number of moves in this game session
		int count = 1;
		while (true) {
			// Display current player board
			System.out.println("Move " + count);
			players[0].showBoard();
			System.out.println();
			// First move by player 1
			oneMove(players[0], players[1]);
			// 2nd move by player 2
			oneMove(players[1], players[0]);

			// Check if any of players is out of ships
			for (Player p : players) {
				if (!p.haveShips()) {
					// If player has no ships left than he lost
					System.out.println("Player " + p.getName() + " lost.");
					return;
				}
			}
			count++;
		}

	}

	// *********************************
	// main method:
	// Create SeaBattle instance and control game flow
	// Precondition:
	// Postcondition:
	// @param Programm arguments
	//
	// @return none
	//
	// *********************************
	public static void main(String[] args) {
		// Create instance of SeaBattle class
		SeaBattle battle = new SeaBattle();
		// Place ships on board
		battle.setupBoard();
		// Play game
		battle.playGame();
		// Show opponent board
		battle.showFinalBoard();
		System.out.println("Game over");
	}

	
	// *********************************
	// setupBoard method:
	// Place ships on each player board
	// Precondition: player array initialized
	// Postcondition: ships placed each player battlefield
	// @param none
	//
	// @return none
	//
	// *********************************
	private void setupBoard() {
		for (Player p : players) {
			System.out.println("Place ships for player " + p.getName());
			p.placeShips();
		}
	}
	// *********************************
	// showFinalBoard method:
	// Display opponent board
	// Precondition: Player #1 initialized
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************

	private void showFinalBoard() {
		System.out.println("Player " + players[1].getName() + " board:");
		players[1].showBoard();

	}

}
