package seabattle;
//**********************************************************
// interface Player
//
// Represents generic methods required by each implementation of player
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public interface Player {
	// *********************************
	// placeShips method:
	// Place ships on player battlefield
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	// 
	// *********************************
	public void placeShips();
	
	// *********************************
	// getNextMove method:
	// Get next move coordinates from this player
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	// 
	// *********************************
	public Move getNextMove();
	
	// *********************************
	// testMove method:
	// Test opponent move on this player battlefield
	// Precondition:
	// Postcondition:
	// @param m  - move coordinates
	//
	// @return updated Move object with hit/miss result
	// 
	// *********************************
	public Move testMove (Move m) throws Exception;
	
	// *********************************
	// haveShips method:
	// Check if player has any ships left
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return true if player has any ships left
	// 
	// *********************************
	
	public boolean haveShips();
	
	// *********************************
	// showBoard method:
	// Display player board
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	// 
	// *********************************
	public void showBoard();
	
	// *********************************
	// getName method:
	// Get player name
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return Player name
	// 
	// *********************************

	public String getName();
	// *********************************
	// recordMove method:
	// Update opponent view board with move results
	// Precondition:
	// Postcondition:
	// @param m - current move information including result from opponent
	//
	// @return none
	// 
	// *********************************
	public void recordMove(Move m);
}
