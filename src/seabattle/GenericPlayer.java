package seabattle;

//**********************************************************
//Class KeyboardPlayer
//
//Implements Player interface and has methods shared by computer and keyboard player classes
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public abstract class GenericPlayer implements Player {

	// Each player store its ships on battlefield instance
	// It has player ships and opponent moves (hit/miss)
	private BattleField myBattleField = new BattleField();
	// Each player also track moves on instance of BattleFieldStatus
	// It only track player moves (hit/miss)
	private BattleFieldStatus opponentBattleStatus = new BattleFieldStatus();
	// Player name
	private String name;
	// Next array control how many ships of each size (1,2,3,4)
	// will be on the fields
	// Index 0 represents number of ships of size 1
	// Index 1 has number of ships of size 2
	// This array define that we have ships with size from 1 to 4 cells
	// We have 4 ships with size 1
	// 3 with size 2
	// 2 with size 3
	// 1 with size 4
	private int[] shipInfo = { 4/* size 1 */, 3/* size 2 */, 2/* size 3 */, 1/* size 4 */ };
	
	//For testing we reduce number of ship to  one with size 4 cells
	//private static int[] shipInfo = { 0/* size 1 */, 0/* size 2 */, 0/* size 3 */, 1/* size 4 */ };
	// *********************************
	// constructor method:
	// Create new instance with given player name
	// Precondition:
	// Postcondition:
	// @param name - player name
	//
	// @return none
	//
	// *********************************
	GenericPlayer(String name) {
		this.name = name;
	}

	// *********************************
	// getNextMove method:
	// Get next move coordinates - abstact
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return new move object with coordinates filled
	//
	// *********************************
	public abstract Move getNextMove();

	// *********************************
	// getMyBattleField method:
	// return this player battlefield object
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return battlefield object
	//
	// *********************************
	protected BattleField getMyBattleField() {
		return myBattleField;
	}

	// *********************************
	// getOpponentBattleField method:
	// return opponent player battlefield object
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return battlefield object
	//
	// *********************************
	protected BattleFieldStatus getOpponentBattleField() {
		return opponentBattleStatus;
	}
	// *********************************
	// testMove method:
	// Test opponent move on this player battlefield
	// Precondition:
	// Postcondition:
	// @param m - move coordinates
	//
	// @return updated Move object with hit/miss result
	//
	// *********************************

	public Move testMove(Move m) throws Exception {
		myBattleField.testHit(m);
		return m;
	}

	// *********************************
	// placeShips method:
	// Place ships on player battlefield
	// Loop over shipInfo array and place required number of ships for each size
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************
	@Override
	public void placeShips() {
		// ship number/index on the battlefield
		int id = 1;
		// shipInfo define number of ships of each size
		for (int size = 0; size < shipInfo.length; size++) {
			// Number of ships
			int n = shipInfo[size];
			// For current size place required number of ships
			// size +1 is number of cells for ship
			for (int i = 0; i < n; i++) {
				placeOneShip(size + 1, id++);
			}
		}
	}

	// *********************************
	// placeOneShip method:
	// Place one player ship on the battlefield - abstact
	//
	// Precondition:
	// Postcondition:
	// @param shipSize - number of cells in the ship
	// @param ship - ship number
	//
	// @return none
	//
	// *********************************
	abstract void placeOneShip(int shipSize, int ship);

	// *********************************
	// recordMove method:
	// Update opponent view board with move results
	// This is player view of opponent board
	// Precondition:
	// Postcondition:
	// @param m - current move information including result from opponent
	//
	// @return none
	//
	// *********************************
	public void recordMove(Move m) {
		try {
			// Update our view of opponent battlefield
			if (m.getStatus() == MoveStatus.MISS)
				opponentBattleStatus.markMiss(m);
			else
				opponentBattleStatus.markHit(m);
		} catch (Exception e) {
			System.out.println("Failed to update status for Move " + m + " player " + this);
		}
	}

	// *********************************
	// haveShips method:
	// Check if player has any ships left - delegate to our battlefield instance
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return true if player has any ships left
	//
	// *********************************
	public boolean haveShips() {
		return myBattleField.haveShips();
	}

	// *********************************
	// showBoard method:
	// Display player board
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************

	public void showBoard() {
		int n = myBattleField.getDimension();
		// Display header
		showHeader();
		// For each row
		for (int row = 0; row < n; row++) {
			// Display player status
			showSingleRow(myBattleField, row);
			System.out.print("| ");
			// display opponent status
			showSingleRow(opponentBattleStatus, row);
			System.out.println("|");
		}
		showFooter();
	}

	// *********************************
	// showHeader method:
	// Display player board header row
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************

	private void showHeader() {
		int n = myBattleField.getDimension();
		for (int player = 0; player < 2; player++) {
			System.out.print("  |");
			for (int i = 0; i < n; i++) {
				System.out.print(i);
			}
			System.out.print("| ");
		}
		System.out.println();
		showFooter();
	}
	// *********************************
	// showFooter method:
	// Display player board footer row
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************

	private void showFooter() {
		int n = myBattleField.getDimension();
		for (int player = 0; player < 2; player++) {
			System.out.print("  |");
			for (int i = 0; i < n; i++) {
				System.out.print('-');
			}
			System.out.print("| ");
		}
		System.out.println();
	}

	// *********************************
	// showSingleRow method:
	// Display single row information
	// Precondition:
	// Postcondition:
	// @param field - battlefield to display
	// @param row - row to display
	//
	// @return none
	//
	// *********************************
	private void showSingleRow(BattleFieldStatus field, int row) {
		int n = field.getDimension();
		System.out.print(" " + row + "|");
		for (int col = 0; col < n; col++) {
			System.out.print(field.getCellStatusLetter(row, col));
		}
	}

	// *********************************
	// getName method:
	// Get player name
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return Player name
	//
	// *********************************
	public String getName() {
		return name;
	}
	// *********************************
	// toString method:
	// return  player name
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return Player name
	//
	// *********************************
	public String toString() {
		return name;
	}
}
