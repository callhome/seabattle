package seabattle;

//**********************************************************
//Class BattleField
//
//represents player battlefield
// Adds methods to place ships and test if move was hit or miss
// Used to track status of each player own battlefield
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************
public class BattleField extends BattleFieldStatus {

	// current ship index on this battlefield
	private int shipID = 0;

	// *********************************
	// placeShip method:
	// place ship on battlefield
	//
	// Precondition:
	// Postcondition:
	// @param row - row coordinate
	// @param col - column coordinate
	// @param isVertical - ship direction vertical or horizontal
	// @param size - number of cells in the ship
	// @param validateOnly - we only validate ship placement without updating battlefield cell status
	//
	// @return none
	//
	// *********************************
	public void placeShip(int row, int col, boolean isVertical, int size, boolean validateOnly) throws Exception {
		//System.out.println("Place ship (" + row + "," + col + ") size=" + size + " vert=" + isVertical);
		//For number of cells in the ship
		for (int i = 0; i < size; i++) {
			// Check if cell is empty
			if (this.isEmpty(row, col)) {
				//If we are not validating, than place new cell ship object
				if (!validateOnly) {
					CellStatusShip ship = new CellStatusShip(shipID, i);
					updateCell(row, col, ship);
				}
				// Update next cell coordinates based on direction
				if (isVertical)
					row++;
				else
					col++;
			} else {
				throw new Exception("Invalid location");
			}
		}
		//If not validating then increase ship id
		if (!validateOnly) 
			shipID++;
	}
	// *********************************
	// testHit method:
	// Test if ship was hit by current opponent move
	//
	// Precondition:
	// Postcondition:
	// @param m - Move instance with coordinates
	//
	// @return Updated move instance with hit/miss status
	//
	// *********************************
	public Move testHit(Move m) throws Exception {
		if (!isEmpty(m.getRow(), m.getCol())) {
			m.setStatus(MoveStatus.HIT);
			markHit(m);
		} else {
			m.setStatus(MoveStatus.MISS);
			markMiss(m);
		}
		return m;
	}
}
