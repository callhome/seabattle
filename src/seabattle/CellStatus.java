package seabattle;

//**********************************************************
//Class CelStatus
//
//represents battlefield cell not occupied by ship
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************

public class CellStatus {
	// Constants to empty/hit cells
	private static final CellStatus EMPTY = new CellStatus();
	// private static final CellStatus HIT = new CellStatus(true);

	// flag if cell was hit
	private boolean hit = false;
	// flag if cell has ship
	private boolean hasShip = false;

	// *********************************
	// constructor method:
	// create new instance
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************
	public CellStatus() {

	}

	// *********************************
	// constructor method:
	// create new instance with specified hit status
	//
	// Precondition:
	// Postcondition:
	// @param wasHit - was this cell hit ?
	//
	// @return none
	//
	// *********************************
	public CellStatus(boolean wasHit) {
		this.hit = wasHit;
	}

	// *********************************
	// constructor method:
	// create new instance with specified hit status and hasShip flag
	//
	// Precondition:
	// Postcondition:
	// @param wasHit - was this cell hit ?
	// @param hasShip - this cell has ship ?
	//
	// @return none
	//
	// *********************************
	public CellStatus(boolean wasHit, boolean hasShip) {
		this.hit = wasHit;
		this.hasShip = hasShip;
	}

	// *********************************
	// isEmpty method:
	// return true if cell is empty ( no ship)
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return true if cell empty
	//
	// *********************************
	public boolean isEmpty() {
		return !hasShip;
	}

	// *********************************
	// wasHit method:
	// accessor - true if cell was hit
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return true if cell was hit
	//
	// *********************************
	public boolean wasHit() {
		return hit;
	}

	// *********************************
	// getEmptyCell method:
	// accessor - return empty cell instance
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return empty cell instance
	//
	// *********************************
	public static CellStatus getEmptyCell() { 
		return EMPTY;
	}

	// *********************************
	// hasShip method:
	// accessor if cell has ship
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return true if cell has ship
	//
	// *********************************
	public boolean hasShip() {
		return hasShip;
	}

	// *********************************
	// setHit method:
	// For empty cell we don't want to set hit status
	// Child class will override to properly handle hit status
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return none
	//
	// *********************************
	public void setHit() {
	}

	// *********************************
	// setHit method:
	// This is accessor for child class to set hit status
	//
	// Precondition:
	// Postcondition:
	// @param value - boolean if cell was hit
	//
	// @return none
	//
	// *********************************
	protected void setHit(boolean value) {
		hit = value;
	}

	// *********************************
	// getCellStatusLetter method:
	// Return cell letter representation
	// account for ship and empty cell been hit or not
	//
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return letter representing cell status
	//
	// *********************************
	public char getCellStatusLetter() {
		if (hit) {
			if (hasShip)
				return 'X';
			else
				return '*';
		} else {
			if (hasShip)
				return '0';
			else
				return ' ';
		}
	}
}
