package seabattle;

//**********************************************************
//Class ComputerPlayer
//
//Extends GenericPlayer and implements methods for ship placement and 
// next move coordinate generation by computer
//
//Name:Alice Shlykov
//
//Date: 5/10/2018
//
//**********************************************************

public class ComputerPlayer extends GenericPlayer {

	// *********************************
	// constructor method:
	// Create new instance with given player name
	// Precondition:
	// Postcondition:
	// @param name - player name
	//
	// @return none
	//
	// *********************************
	ComputerPlayer(String name) {
		super(name);
	}

	// *********************************
	// placeOneShip method:
	// Place one player ship on the battlefield
	// Generate random coordinates till we hit valid cell
	//
	// Precondition:
	// Postcondition:
	// @param shipSize - number of cells in the ship
	// @param ship - ship number
	//
	// @return none
	//
	// *********************************
	void placeOneShip(int shipSize, int ship) {
		
		// Loop to deal with invalid coordinates
		while (true) {
			try {
				// Get two randmo number 
				int row = (int) (Math.random() * BattleFieldStatus.getDimension());
				int col = (int) (Math.random() * BattleFieldStatus.getDimension());
				int direction = (int) (Math.random() * 2);
				// First try to place ship without marking battlefield cells
				// In case we collide with another ship or run out of board
				// it will generate exception and we restart loop asking for new coordinates
				getMyBattleField().placeShip(row, col, direction == 0, shipSize, true);
				// If we got there then entered coordinates are valid
				// and we will mark board cells with ship number
				getMyBattleField().placeShip(row, col, direction == 0, shipSize, false);
				return;
			} catch (Exception e) {

			}
		}
	}

	// *********************************
	// getNextMove method:
	// Get next move coordinates
	// Generate random coordinates till we get valid set and we have not tried these
	// before
	// Precondition:
	// Postcondition:
	// @param none
	//
	// @return new move object with coordinates filled
	//
	// *********************************
	@Override
	public Move getNextMove() {
		// hard limit for number of tries
		int count = 20000;
		while (count > 0) {
			//Generate random coordinates
			int x = (int) (Math.random() * BattleFieldStatus.getDimension());
			int y = (int) (Math.random() * BattleFieldStatus.getDimension());
			try {
				// Check if we already tried these coordinates before using opponent battlefield
				// If not then return new Move object
				if (getOpponentBattleField().isEmpty(x, y) && !getOpponentBattleField().wasHit(x, y))
					return new Move(x, y);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			count--;
		}
		// Can't fine anything with random then find any empty cell
		return getOpponentBattleField().findEmptyCell();
	}


}
